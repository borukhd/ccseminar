""" Transitive-Inference model implementation.
"""
import ccobra
import random
import repNode as n
import numpy as np



class Linear(ccobra.CCobraModel):
    """ TransitivityInt CCOBRA implementation.
    """
    def __init__(self, name='Linear'):
        """ Initializes the TransitivityInt model.
        Parameters
        ----------
        name : str
            Unique name of the model. Will be used throughout the ORCA
            framework as a means for identifying the model.
        """
        self.ref = [] #[n.Node('referencePoint')]
        super(Linear, self).__init__(name, ['spatial-relational'], ['single-choice'])

    def predict(self, item, **kwargs):
        """ Predicts weighted responses to a given syllogism.
        """ 
        # Generate and return the current prediction
        first, second = n.orderedItem(item)
        for node in self.ref:
            if node.name == first:
                if second in [a.name for a in node.smallerreachable()]:
                    return int(first)
                if second in [a.name for a in node.largerreachable()]:
                    return int(second)
            if node.name == second:
                if first in [a.name for a in node.largerreachable()]:
                    return int(first)
                if first in [a.name for a in node.smallerreachable()]:
                    return int(second)
        return int([first, second][np.random.randint(0, len([first, second]))])

    def adapt(self, item, target, **kwargs):
        la, sm = n.orderedItem(item)
        nodela, nodesm = None, None
        oc1, oc2 = False, False
        for oldNode1 in self.ref:
            if sm == oldNode1.name:
                nodesm = oldNode1
                for oldNode2 in self.ref:
                    if la == oldNode2.name:
                        nodela = oldNode2
                        oldNode1.addLargerNode(oldNode2)
                        oc1 = True
        for oldNode1 in self.ref:
            if la == oldNode1.name:
                nodela = oldNode1
                for oldNode2 in self.ref:
                    if sm == oldNode2.name:
                        nodesm = oldNode2
                        oldNode1.addSmallerNode(oldNode2)
                        oc2 = True
        if not sm in [a.name for a in self.ref]:
            nodesm = n.Node(sm)
            self.ref.append(nodesm)
        if not la in [a.name for a in self.ref]:
            nodela = n.Node(la)
            self.ref.append(nodela)
        #if oc1 and oc2 and int(la) - int(sm) < -1:
            #print('transitive inference:', la, sm, '--- first number < second number:', nodesm in nodela.smallerreachable())

    def visualize_structure(self, given):
        la, sm = given.task[0][0], given.task[1][0]
        outstring = " given: " + la + ', ' + sm + "\n"
        for item in self.ref:
            for reach in item.reachable():
                reach = item
                if reach.name == la:
                    outstring += str(item.smallerreachable()) + ' - ' + la + ' - ' + str(item.largerreachable()) + '\n'
                if reach.name == sm:
                    outstring += str(item.smallerreachable()) + ' - ' + sm + ' - ' + str(item.largerreachable()) + '\n'