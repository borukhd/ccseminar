
""" Transitive-Inference model implementation.
"""
import ccobra
import random
import valueTransferTheory

class TransitivityInt(valueTransferTheory.VTTInt):
    """ TransitivityInt CCOself.BRA implementation.
    """
    def __init__(self, name='VTT2'):
        """ Initializes the TransitivityInt model.
        Parameters
        ----------
        name : str
            Unique name of the model. Will be used throughout the ORCA
            framework as a means for identifying the model.
        """
        super(TransitivityInt, self).__init__(name)
        self.a = 0.4    #WEIGHTINGFACTOR

    def pre_train(self, dataset):
        self.itemsLength = dataset[0][0]['length']
        for ind in range(self.itemsLength-1, -1, -1):
            i_minus_1, i, i_plus_1 = ind + 1, ind, ind - 1
            self.V[self.items[i]] = self.R[self.items[i]] 
            if self.itemsLength - 1 > i_minus_1:        
                self.V[self.items[i]] += self.a*self.getV(self.items[i_minus_1])