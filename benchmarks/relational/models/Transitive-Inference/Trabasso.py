""" Transitive-Inference model implementation.
"""
import ccobra
import random
import repNode as n
import numpy as np

def setEq(a, b):
    if len([i for i in a if a not in b]) == 0 and len([i for i in b if b not in a]) == 0:
        return True
    return False

class Trabasso(ccobra.CCobraModel):
    """ TransitivityInt CCOBRA implementation.
    """
    def __init__(self, name='Trabasso'):
        """ Initializes the TransitivityInt model.
        Parameters
        ----------
        name : str
            Unique name of the model. Will be used throughout the ORCA
            framework as a means for identifying the model.
        """
        #Done; various other interpretations conceivable 
        self.intArrs = [] 
        self.hardnessMiddle = 10 
        super(Trabasso, self).__init__(name, ['spatial-relational'], ['single-choice'])

    def predict(self, item, **kwargs):
        """ Predicts weighted responses to a given syllogism.
        """ 
        # Generate and return the current prediction
        first, second = item.choices[0][0][0], item.choices[1][0][0]
        for array in self.intArrs:
            if first in array and second in array:
                if array.index(first) < array.index(second):
                    chance = random.random()
                    if chance > array.index(first)/(len(array)*self.hardnessMiddle):
                        return int(first)
                    if chance > (len(array) - array.index(second))/(len(array)*self.hardnessMiddle):
                        return int(first)
                elif array.index(second) < array.index(first):
                    chance = random.random()
                    if chance > array.index(second)/(len(array)*self.hardnessMiddle):
                        return int(second)
                    if chance > (len(array) - array.index(first))/(len(array)*self.hardnessMiddle):
                        return int(second)
        return int([first, second][np.random.randint(0, len([first, second]))])

    def adapt(self, item, target, **kwargs):
        first, second = self.sortedPair((item.choices[0][0][0], item.choices[1][0][0]))
        pairsFound = 0
        for array in self.intArrs:
            if first in array and second in array:
                pairsFound += 1
                if array.index(first) < array.index(second):
                    return
                if array.index(first) > array.index(second):
                    array.remove(first)
                    array.remove(second)
            elif first in array:
                for otherArray in self.intArrs:
                    if second in otherArray:
                        if array.index(first) == len(array)-1 and 0 == otherArray.index(second):
                            pairsFound += 1
                            array += otherArray[1:]
                            newArrs = []
                            for eachArray in self.intArrs:
                                if not setEq(eachArray, otherArray):
                                    newArrs.append(eachArray)
                            self.intArrs = newArrs
                            continue
            elif second in array:
                for otherArray in self.intArrs:
                    if first in otherArray:    
                        if otherArray.index(first) == len(array)-1 and 0 == array.index(second):
                            pairsFound += 1
                            otherArray += array[1:]
                            newArrs = []
                            for eachArray in self.intArrs:
                                if not setEq(eachArray, otherArray):
                                    newArrs.append(array)
                            self.intArrs = newArrs
                            continue
        if pairsFound == 0:
            self.intArrs.append([first, second])