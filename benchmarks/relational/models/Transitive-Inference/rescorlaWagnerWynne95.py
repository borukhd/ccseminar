
""" Transitive-Inference model implementation.
"""
import ccobra
import random
import math

class TransitivityInt(ccobra.CCobraModel):
    """ TransitivityInt CCOself.BRA implementation.
    """
    def __init__(self, name='rescorlaWagnerWynne95'):
        """ Initializes the TransitivityInt model.
        Parameters
        ----------
        name : str
            Unique name of the model. Will be used throughout the ORCA
            framework as a means for identifying the model.
        """
        #Done except for Vz; Vz set to 0 as in Lazareva
        self.vInit = 'random'   #init associative values
        self.vInit = 1
        self.B = 0.1            #CONSTANTREWARD
        self.a = 0.1            #SCALINGPARAMETER
        self.assocV = {}
        super(TransitivityInt, self).__init__(name, ['spatial-relational'], ['single-choice'])


    def predict(self, item, **kwargs):
        """ Predicts weighted responses to a given syllogism.
        """ 
        # Generate and return the current prediction
        pair = item.choices[0][0][0], item.choices[1][0][0]
        first, second = pair
        probabilityOfFirst = self.rw_probability(first, pair)
        if random.random() < probabilityOfFirst:
            return int(first)
        return int(second)

    def person_train(self, data):
        item = data[0]['item']
        target = data[0]['target']
        adapt(self, item, target)

    def adapt(self, item, target, **kwargs):
        pair = item.choices[0][0][0], item.choices[1][0][0]
        first, second = self.sortedPair(pair)
        self.assocVinit(pair)
        self.assocV[first] += self.B*(1-(self.assocV[first] + self.assocV[second]))
        self.assocV[second] -= self.B*(self.assocV[second] + self.assocV[first])

    def rw_probability(self, elem, pair):
        first, second = pair
        self.assocVinit(pair)
        first, second = elem, [a for a in pair if a != elem][0]
        Vz = 0 #Vz LEFT OUT
        r = (self.assocV[first] + Vz) / (self.assocV[first] + self.assocV[second] + Vz)
        return 1 / math.exp((-1*self.a*(2*r-1)))

    def assocVinit(self, first, second = None):
        if isinstance(first, tuple):
            first, second = first
        for each in [first, second]:
            if not str(each) in self.assocV.keys() and each != None:
                self.assocV[str(each)] = random.random()
                if self.vInit != 'random':
                    self.assocV[str(each)] = float(self.vInit)