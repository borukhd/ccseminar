def setequal(a, b):
    for node in a:
        if not node in b:
            False
    for node in b:
        if not node in a:
            False
    return True

def rewardedStimulus(pair, stim = None):
    if stim:
        return min([int(a) for a in pair]) == int(stim)
    return min([int(a) for a in pair])

def orderedPair(pair):
    return min([int(a) for a in pair]), max([int(a) for a in pair])

def orderedItem(item):
    return min([int(a) for a in [item.choices[0][0][0], item.choices[1][0][0]]]), max([int(a) for a in [item.choices[0][0][0], item.choices[1][0][0]]])

class Node:
    allNodes = 0
    def __init__(self, namestring = 'noname'):
        self.number = Node.allNodes
        Node.allNodes += 1
        self.name = namestring
        self.smallerNodes = set()
        self.largerNodes = set()
        self.nodeDistance = {}      # +larger, -smaller
        
    def __eq__(self, value):
        return self.name == value.name

    def __hash__(self):
        return int(self.name)
        
    def __str__(self):
        return "name: " + self.name #+ ", number: " + str(self.number) + ' Before: ' + str(self.smallerNodes) + ' After: ' + str(self.largerNodes)

    def __repr__(self):
        return str(self)

    def addSmallerNode(self, node, distance = -1):
        self.addNodeWithDistance(distance, node)
        node.addNodeWithDistance((-1)*distance, self)
    
    def addLargerNode(self, node, distance = 1):
        self.addNodeWithDistance(distance, node)
        node.addNodeWithDistance((-1)*distance, self)

    def addNodeWithDistance(self, distance, node = None):
        if not node:
            node = Node()
        self.nodeDistance[node.name] = distance
        if distance > 0:
            if node in self.smallerNodes:
                print('circle duplicate not added:', node)
            else:
                for oldNode in self.largerNodes:
                    if oldNode.name == node.name:
                        return
                self.largerNodes.add(node)
        elif distance < 0:
            if node in self.largerNodes:
                print('circle duplicate not added:', node)
            else:
                for oldNode in self.smallerNodes:
                    if oldNode.name == node.name:
                        return
                self.smallerNodes.add(node)
        
    def stepReachable(self):
        return {self} | self.largerNodes | self.smallerNodes

    def reachable(self):
        lastReachable = set()
        lastReachable.add(self)
        currentReachable = lastReachable.copy()
        for a in lastReachable:
            currentReachable |= a.stepReachable()
        while not setequal(lastReachable, currentReachable):
            for node in lastReachable:
                lastReachable |= node.stepReachable()
                currentReachable |= node.stepReachable()
            for node in lastReachable:
                currentReachable |= node.stepReachable()
        return currentReachable

    def smallerreachable(self):
        lastReachable = set()
        lastReachable.add(self)
        currentReachable = lastReachable.copy()
        for a in lastReachable:
            currentReachable |= {a} | a.smallerNodes
        while not setequal(lastReachable, currentReachable):
            for node in lastReachable:
                lastReachable |= {self} | self.smallerNodes
                currentReachable |= {self} | self.smallerNodes
            for node in lastReachable:
                currentReachable |= {self} | self.smallerNodes
        return currentReachable - {self}

    def smallerreachableIncl(self):
        lastReachable = set()
        lastReachable.add(self)
        currentReachable = lastReachable.copy()
        for a in lastReachable:
            currentReachable |= {a} | a.smallerNodes
        while not setequal(lastReachable, currentReachable):
            for node in lastReachable:
                lastReachable |= {self} | self.smallerNodes
                currentReachable |= {self} | self.smallerNodes
            for node in lastReachable:
                currentReachable |= {self} | self.smallerNodes
        return currentReachable

    def largerreachable(self):
        lastReachable = set()
        lastReachable.add(self)
        currentReachable = lastReachable.copy()
        for a in lastReachable:
            currentReachable |= {a} | a.largerNodes
        while not setequal(lastReachable, currentReachable):
            for node in lastReachable:
                lastReachable |= {self} | self.largerNodes
                currentReachable |= {self} | self.largerNodes
            for node in lastReachable:
                currentReachable |= {self} | self.largerNodes
        return currentReachable - {self}

    def largerreachableIncl(self):
        lastReachable = set()
        lastReachable.add(self)
        currentReachable = lastReachable.copy()
        for a in lastReachable:
            currentReachable |= {a} | a.largerNodes
        while not setequal(lastReachable, currentReachable):
            for node in lastReachable:
                lastReachable |= {self} | self.largerNodes
                currentReachable |= {self} | self.largerNodes
            for node in lastReachable:
                currentReachable |= {self} | self.largerNodes
        return currentReachable