
""" Transitive-Inference model implementation.
"""
import ccobra
import random

def rewardedStimulus(stim, pair):
    return min([int(a) for a in pair]) == int(stim)

class TransitivityInt(ccobra.CCobraModel):
    """ TransitivityInt CCOself.BRA implementation.
    """
    def __init__(self, name='SiemannDelius'):
        """ Initializes the TransitivityInt model.
        Parameters
        ----------
        name : str
            Unique name of the model. Will be used throughout the ORCA
            framework as a means for identifying the model.
        """
        #Completed, parameters not optimized
        self.Bplus = 0.7            #CONSTANTBETAREWARD
        self.Bminus = 0.7            #CONSTANTBETA-NON-REWARD
        self.e = 0.8            #ELEMENTALWEIGHT
        self.k = 1 - self.e     #CONFIGURALWEIGHT  
        self.elemVinit = 0.1
        self.confVinit = 0.1
        self.elemV = {}
        self.confV = {}
        super(TransitivityInt, self).__init__(name, ['spatial-relational'], ['single-choice'])

    def predict(self, item, **kwargs):
        """ Predicts weighted responses to a given syllogism.
        """ 
        # Generate and return the current prediction
        pair = item.choices[0][0][0], item.choices[1][0][0]
        prediction = self.siemann_delius_predict(pair)
        self.siemann_delius_process(pair)
        return prediction

    def siemann_delius_process(self, pair):
        first, second = pair
        for item in pair:
            if not (item, pair) in self.confV.keys():
                self.confV[item, pair] = self.confVinit
                self.confV[item, (second, first)] = self.confVinit
        if rewardedStimulus(first, pair):
            self.elemV[first] += (self.Bplus* self.elemV[first]*self.sd_probability(first, pair)) * self.e
            self.confV[first, pair] += (self.Bplus * self.confV[first, pair] * self.sd_probability(first, pair) * self.k)
            self.elemV[second] -= (self.Bminus* self.elemV[second]*(1 - self.sd_probability(first, pair))) * self.e
            self.confV[second, pair] = self.confV[first, pair] - (self.Bminus * self.confV[first, pair] * (1 - self.sd_probability(first, pair)) * self.k)
        elif rewardedStimulus(second, pair):
            self.elemV[second] += (self.Bplus* self.elemV[second]*self.sd_probability(second, pair)) * self.e
            self.confV[second, pair] += (self.Bplus * self.confV[second, pair] * self.sd_probability(second, pair) * self.k) 
            self.elemV[first] -= (self.Bminus* self.elemV[first]*(1 - self.sd_probability(second, pair))) * self.e
            self.confV[first, pair] = self.confV[second, pair] - (self.Bminus * self.confV[second, pair] * (1 - self.sd_probability(second, pair)) * self.k)
        
    def siemann_delius_predict(self, pair):
        first, second = pair
        probabilityOfFirst = self.sd_probability(first, pair)
        if random.random() < probabilityOfFirst:
            return int(first)
        return int(second)

    def sd_predict_firstTest(self, item, pair):
        first, second = pair 
        return self.elemV[first]/(self.elemV[first] + self.elemV[second])
    
    def sd_probability(self, item, pair):
        first, second = pair
        for element in pair:
            if not element in self.elemV.keys():
                self.elemV[element] = self.elemVinit
            first, second = second, first
        if not (item, pair) in self.confV.keys():
            return self.sd_predict_firstTest(item, pair)
        return (self.elemV[first] * self.confV[first, pair])/(self.elemV[first] * self.confV[first, pair] + self.elemV[second] * self.confV[second, pair])
